#Retrolambda
-dontwarn java.lang.invoke.*
-dontwarn **$$Lambda$*

#Squareup (Okhttp, Retrofit, Converters)
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**

#Picasso
-dontwarn com.squareup.okhttp.**

#JSOUP
-keeppackagenames org.jsoup.nodes

#ButterKnife
-keep class butterknife.*
-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }

#GOOGLE INFO
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
-allowaccessmodification
-dontpreverify
# The remainder of this file is identical to the non-optimized version
# of the Proguard configuration file (except that the other file has
# flags to turn off optimization).
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-keepattributes *Annotation*
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService
# For native methods
-keepclasseswithmembernames class * {
    native <methods>;
}
# keep setters in Views so that animations can still work.
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}
# For enumeration classes
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}
-keepclassmembers class **.R$* {
    public static <fields>;
}
# Support for old versions
-dontwarn android.support.**


#UNCHECKED BLOCK WITH RULES FUNDED AT STACK \ etc (!)
#
##FIREBASE v2
#-keep class com.firebase.** { *; }
#-keepnames class com.fasterxml.jackson.** { *; }
#-keepnames class javax.servlet.** { *; }
#-keepnames class org.ietf.jgss.** { *; }
#-dontwarn org.w3c.dom.**
#-dontwarn org.joda.time.**
#-dontwarn org.shaded.apache.**
#-dontwarn org.ietf.jgss.**
#-dontwarn com.firebase.**
#-dontnote com.firebase.client.core.GaePlatform
#
##OKHTTP
#-dontwarn okhttp3.**
#-dontwarn okio.**
#-dontwarn javax.annotation.**
#
##RETROFIT
#-dontwarn okio.**
#-dontwarn retrofit2.**
#-keep class retrofit2.** { *; }
#-keepattributes Signature
#-keepattributes Exceptions
#-keepclasseswithmembers class * { @retrofit2.http.* <methods>; }
#
#
#
##BUTTER KNIFE
#-keep public class * implements butterknife.Unbinder { public <init>(**, android.view.View); }
#-keep class butterknife.*
#-keepclasseswithmembernames class * { @butterknife.* <methods>; }
#-keepclasseswithmembernames class * { @butterknife.* <fields>; }
#
##GAPS
## For Google Play Services
#-keep public class com.google.android.gms.ads.**{
#   public *;
#}
#
## For old ads classes
#-keep public class com.google.ads.**{
#   public *;
#}
#
## For mediation
#-keepattributes *Annotation*
#
## Other required classes for Google Play Services
## Read more at http://developer.android.com/google/play-services/setup.html
#-keep class * extends java.util.ListResourceBundle {
#   protected Object[][] getContents();
#}
#
#-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
#   public static final *** NULL;
#}
#
#-keepnames @com.google.android.gms.common.annotation.KeepName class *
#-keepclassmembernames class * {
#   @com.google.android.gms.common.annotation.KeepName *;
#}
#
#-keepnames class * implements android.os.Parcelable {
#   public static final ** CREATOR;
#}
#
##RETROLAMBDA
#-dontwarn java.lang.invoke.*
#-dontwarn **$$Lambda$*
#
##WTF
#-dontwarn java.nio.file.Files
#-dontwarn java.nio.file.Path
#-dontwarn java.nio.file.OpenOption
#-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
#
##BILLING
#-keep class com.android.vending.billing.**
#
##RETROFIT
#-dontwarn okio.**
#-dontwarn javax.annotation.**